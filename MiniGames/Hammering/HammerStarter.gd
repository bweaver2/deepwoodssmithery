extends StaticBody2D


signal popup_requested(scene_path)


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
	
# right click to start
func _on_HammerStarter_input_event(viewport, event, shape_idx):
	if  event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_RIGHT:
		emit_signal("popup_requested", "res://MiniGames/Hammering/Hammering.tscn")
