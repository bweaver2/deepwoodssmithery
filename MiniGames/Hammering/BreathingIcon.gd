extends PathFollow2D


var HammerGameOffset = -1
var HammerGameAisle = -1



func update_offset_for_timer(timer):
	var newOffset = timer - HammerGameOffset
	newOffset = newOffset if newOffset > 0  else 0
	set_offset(newOffset * 100)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
