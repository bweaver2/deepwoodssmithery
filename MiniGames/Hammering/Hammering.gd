extends Node2D


@export var beats_per_minute = 24

# Getting the paths in an easy to reach place for adding the breathers
@onready var paths = [
	$CanvasLayer/Panel/ColorRect/Path1,
	$CanvasLayer/Panel/ColorRect/Path2,
	$CanvasLayer/Panel/ColorRect/Path3,
	$CanvasLayer/Panel/ColorRect/Path4,
	$CanvasLayer/Panel/ColorRect/Path5
]

@onready var pointer = $CanvasLayer/Panel/ColorRect/Pointer

var breather_resource = load("res://MiniGames/Hammering/BreathingIcon.tscn")

const MIN_AISLE = 0
const MAX_AISLE = 4
const TOTAL_FALL_TIME = 2.5

var current_aisle = 2

var timer = 0.0

var targets = [
	2,
	2,
	2,
	0,
	1,
	3,
	4
]

# Stores the actual node instances based on the template in targets
var target_nodes = []
var targets_loaded = false

# Called when the node enters the scene tree for the first time.
func _ready():
	for index in range(targets.size()):
		var aisle = targets[index]
		var newNode = breather_resource.instantiate()
		newNode.HammerGameOffset = index
		newNode.HammerGameAisle = aisle
		
		# make sure the newNode is in all the places it needs to be
		# that means in the path itself, and in our quick lookup list here
		var path = paths[aisle]
		path.add_child(newNode)
		target_nodes.push_back(newNode)
	targets_loaded = true
	
	var testNode = breather_resource.instantiate()
	testNode.transform.x = Vector2(100, 0)
	testNode.transform.y = Vector2(0, 0)
	add_child(testNode)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#print(delta)

func remove_target_node(index):
	var node = target_nodes[index]
	target_nodes.remove(index)
	var path = paths[node.HammerGameAisle]
	path.remove_child(node)
	node.call_deferred("free")
	
	if (target_nodes.size() == 0):
		call_deferred("free")
	
	
func update_pointer_icon():
	var new_x = (current_aisle * 20) + 10
	pointer.position.x = new_x
	
	
func update_node_timer_offsets():
	for node in target_nodes:
		node.update_offset_for_timer(timer)


func _physics_process(delta):
	
	# nothing to do here if all if the nodes are gone
	if targets_loaded and target_nodes.size() == 0:
		return
	
	#first try to update the timer and nodes
	var oldTime = timer
	var newTime = timer + delta
	var currentMaxTime = (target_nodes[0].HammerGameOffset) + TOTAL_FALL_TIME
	
	timer = clamp(newTime, 0, currentMaxTime)
	update_node_timer_offsets()
	
	# then try to handle any inputs
	if Input.is_action_just_pressed("ui_select"):
		# try to hammer
		for index in  range(target_nodes.size()):
			var node = target_nodes[index]
			#make sure we only break things at the end
			if(node.get_offset() < 0.98):
				break
				
			if node.HammerGameAisle == current_aisle:
				remove_target_node(index)
				break
	
	if Input.is_action_just_pressed("ui_left") or Input.is_action_just_pressed("ui_up"):
		current_aisle = clamp(current_aisle - 1, MIN_AISLE, MAX_AISLE)
		update_pointer_icon()
		
	if Input.is_action_just_pressed("ui_right") or Input.is_action_just_pressed("ui_down"):
		current_aisle = clamp(current_aisle + 1, MIN_AISLE, MAX_AISLE)	
		update_pointer_icon()
			
