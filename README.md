# README #

This is a small personal project. I wanted to see how hard it is to make a game, even a simple one. Art, music, and code are being handled by hand to get a feel for every aspect of game creation.

This is Deepwoods Smithery, a quiet, slice of life game.

You are a recently graduated Journeyman Smith. Your uncle has invited you out to join him
in founding a town where a mine was discovered. Grow yourself, grow the town, develop the mine,
and pay back the startup funding given to make this all happen.


![DEMO_RECORDING_GIF](./SimpleSmeltingProcess.gif)

### How do I get set up? ###

* Download Godot 3.5.1
* Import the project into Godot
* You can run or edit the game from the main menu

### Who do I talk to? ###

* Brandon Weaver