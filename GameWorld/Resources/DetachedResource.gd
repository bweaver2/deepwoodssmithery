extends CharacterBody2D

@export var item_id = 3
@export var item_count = 1

# var velocity = Vector2.ZERO
const FRICTION = 100
const MIN_DISTANCE = 10
const MAX_SPEED = 80

# Called when the node enters the scene tree for the first time.
func _ready():
	var sprite_details = ItemManager.get_sprites_for_item(item_id);
	$Sprite2D.texture = sprite_details.texture
	$Sprite2D.vframes = sprite_details.vframes
	$Sprite2D.hframes = sprite_details.hframes
	$Sprite2D.frame = sprite_details.sprite_index
	
	var image_size = sprite_details.texture.get_size() #image size
	var desired_size = 16.0
	var x_scale = desired_size / (image_size.x / sprite_details.hframes)
	var y_scale = desired_size / (image_size.y / sprite_details.vframes)
	var scale = Vector2(x_scale, y_scale)
	$Sprite2D.scale = scale


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	# see if any players are nearby
	var players = get_tree().get_nodes_in_group("Player");
	var start_global_position = global_position
	for player in players:
		#check to see if the player has magnet info
		if not "magnet_size" in player: continue
		if not player.can_pickup_items(item_id, item_count): continue
		
		# now see if a any player is in range
		var distance = start_global_position.distance_to(player.global_position)
		if(distance < player.magnet_size and distance > MIN_DISTANCE):
			var base_strength = player.magnet_strength
			var pseudo_distance = abs(distance - (player.magnet_size / 2.0))
			var acceleration = delta * base_strength / (distance * distance)
			
			var dir = (player.position - start_global_position).normalized()
			velocity += dir * acceleration
			velocity = velocity.limit_length(MAX_SPEED * delta)
		
		else:
			velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)	
			
		if distance <= MIN_DISTANCE:
			if player.can_pickup_items(item_id, item_count):
				var left_over = player.pickup_items(item_id, item_count)
				if left_over <= 0:
					call_deferred("free")
				else:
					item_count = left_over
		
		var collision_results = move_and_collide(velocity)
		if collision_results:
			velocity = Vector2.ZERO
			
		
