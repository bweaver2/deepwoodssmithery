extends StaticBody2D

# This class is meant to be a lunaching point for all
# statically placed interctable objects in the game.
# Whether that's crafting stations, chests, doors, 
# or even resource nodes like trees, clay deposits, and mineral deposits

signal popup_requested(scene_path, data_for_popup)

enum interactable_state {
	PASSIVE = 0,
	ACTIVE = 1,
	COMPLETE = 2
}

@export var xcoord = 10
@export var ycoord = 10

# This ID should be in ItemManager.interactable_definitions
@export var interactable_id = 0
@export var state = interactable_state.PASSIVE

# The followding arrays contain ItemContainers detaililng what is actually in the device
# The definition in ItemManager defines expected inputs and outputs
# On break, all of these items will be dropped, in addition to whatever is in the definition
var completed_items = []
var input_items = []

@export var is_breakable = true
@export var health = 3

@export var timer_length = 10
@export var batch_size = 1

var item_container_ref = load("res://Globals/ItemContainer.tscn")
var detached_resource_ref = load("res://GameWorld/Resources/DetachedResource.tscn")
var current_recipe = null
var is_simple = true

# if no item is currently selected, then item_id will be null
# also, if this is a complex interactable, we can always interact with it
func can_interact(item_container):
	if not is_simple:
		return true
	
	match state:
		interactable_state.PASSIVE:
			if item_container.count == 0: 
				return false
			
			var accept_list = ItemManager.get_accepted_items_for_interactable_recipes(interactable_id)
			var found_index = accept_list.find(item_container.item_id)
			
			return found_index >= 0
		interactable_state.ACTIVE:
			return false
		interactable_state.COMPLETE:
			return true
		_:
			return false
	
# this should be overloaded in each child class
func perform_interact(item_container):
	
	if not is_simple:
		open_interactable_popup()
		return
	
	match state:
		interactable_state.PASSIVE:
			var def = ItemManager.interactable_definitions[interactable_id]
			var recipe_ids = def.recipe_ids
			for recipe_id in recipe_ids:
				var recipe = ItemManager.recipes_definitions[recipe_id]
				var count_needed = recipe.item_ids_required.get(item_container.item_id)
				if count_needed == null or item_container.count < count_needed:
					continue
				
				item_container.add_count(-1 * count_needed)
				var new_input = item_container_ref.instantiate()
				new_input.set_items(item_container.item_id, count_needed)
				input_items.append(new_input)
				
				current_recipe = recipe
				
				if(recipe.time_to_make == 0):
					_on_Timer_timeout()
				else:
					state = interactable_state.ACTIVE
					$Timer.start(recipe.time_to_make)
				
		interactable_state.ACTIVE:
			print("Trying to activate during an active cycle is not allowed")
		interactable_state.COMPLETE:
			print("Complete")
			$StorageSlot.visible = false
			state = interactable_state.PASSIVE
			
			# Give the item to the player (or drop it on the ground)
			for completed_item in completed_items:
				var left_over = PlayerData.auto_add_items(completed_item.item_id, completed_item.count)
				if left_over > 0:
					var pos = get_global_transform()
					var detached = detached_resource_ref.instantiate()
					detached.item_id = completed_item.item_id
					detached.item_count = left_over
					detached.transform.x = pos.x
					detached.transform.y = pos.y
					
					var game_root = get_tree().get_nodes_in_group("GameRoot")[0];
					game_root.add_node_to_current_scene(detached)
				
				completed_item.call_deferred("free")
			
			completed_items.clear()
		_:
			print("Unknown State")

func on_interact():
	
	var item_container = PlayerData.selected_item_container()
	
	if not can_interact(item_container):
		# maybe also play a sound? There should be some indication that it's not allowed.
		return
	
	perform_interact(item_container)


# This method is for complex interactables where you need to open a popup
func open_interactable_popup():
	var data_package = {
		"interactable_id": interactable_id,
		"input_array": input_items,
		"output_array": completed_items,
		"timer": $Timer,
		"on_recipe_canceled": self.on_recipe_canceled
	}
	emit_signal("popup_requested", "res://GameWorld/Interactable/InteractablePopup.tscn", data_package)
	pass


# Should drop anything stored inside (inputs or outputs)
# in addition to the items in items_dropped_on_break
func on_break():
	
	# Typically the player needs to be holding a tool to break an object
	var item_container = PlayerData.selected_item_container()
	var item_id = null
	if item_container.count > 0:
		item_id = item_container.item_id
		
	# Make sure we can actually break the item currently
	if not is_breakable or not can_break(item_id) or health == -1:
		return
	
	# To give the player a chance to make a mistake, breaking only happens after several swings
	health = health - 1
	if health <= 0:
		# we should be dropping a lot of items on the ground
		# so we need to get the current level
		# and generate DetachableItems for everything inside
		# we should probably scatter them somehow as well
		var game_root = get_tree().get_nodes_in_group("GameRoot")[0];
		var pos = get_global_transform().origin
		
		for completed_item in self.completed_items:
			var detached = detached_resource_ref.instantiate()
			detached.item_id = completed_item.item_id
			detached.item_count = completed_item.count
			detached.transform = Transform2D(0.0, Vector2(pos.x, pos.y))
			game_root.add_node_to_current_scene(detached)
		
		for input_item in input_items:
			var detached = detached_resource_ref.instantiate()
			detached.item_id = input_item.item_id
			detached.item_count = input_item.count
			detached.transform = Transform2D(0.0, Vector2(pos.x, pos.y))
			game_root.add_node_to_current_scene(detached)
			
		# The item manager defines what gets dropped on break as well
		# But each entry in the array  could be a single ID, or a dictionary
		var def = ItemManager.interactable_definitions[interactable_id]
		var dropped_on_break = def.dropped_on_destroy
		for broken_item in dropped_on_break:
			if typeof(broken_item) == TYPE_INT:
				var detached = detached_resource_ref.instantiate()
				detached.item_id = broken_item
				detached.item_count = 1
				detached.transform = Transform2D(0.0, Vector2(pos.x, pos.y))
				game_root.add_node_to_current_scene(detached)
			else:
				for broken_id in broken_item:
					var detached = detached_resource_ref.instantiate()
					detached.item_id = broken_id
					detached.item_count = broken_item[broken_id]
					detached.transform = Transform2D(0.0, Vector2(pos.x, pos.y))
					game_root.add_node_to_current_scene(detached)
		
		call_deferred("free")
	
	# Might have a few as a leadup to the actual break?
	print("Play breaking sound")
	
func can_break(item_id):
	return true

func on_recipe_canceled():
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	position.x = 16 * xcoord
	position.y = 16 * ycoord
	is_simple = ItemManager.get_is_simple_interactable(interactable_id)
	var sprite_details = ItemManager.get_details_for_interactable(interactable_id)
	var sprite_height = sprite_details.texture.get_height()
	var sprite_width = sprite_details.texture.get_width()
	var frame_height = sprite_height / sprite_details.vframes
	var frame_width = sprite_width / sprite_details.hframes
	
	# Get the frames for the animated sprite, and clear all default animations
	var frames = SpriteFrames.new()
	$AnimatedSprite2D.frames = frames
	$AnimatedSprite2D.transform = Transform2D(0, Vector2(0, -1.0 * frame_height / 2.0))
	frames.clear_all() 
	
	# add the new default frame
	if not frames.has_animation("default"):
		frames.add_animation_library("default")
	
	var default_frame = AtlasTexture.new()
	default_frame.set_atlas(sprite_details.texture)
	var default_row = sprite_details.sprite_index / sprite_details.hframes
	var default_col = sprite_details.sprite_index % sprite_details.hframes
	default_frame.set_region(Rect2( default_col * frame_width, default_row * frame_height, frame_width, frame_height ))
	frames.add_frame("default", default_frame)
	
	
	# add the new active frames
	if not frames.has_animation("active"):
		frames.add_animation("active")
		
	for active_index in sprite_details.active_sprite_indexes:
		var active_frame = AtlasTexture.new()
		active_frame.set_atlas(sprite_details.texture)
		var active_row = active_index / sprite_details.hframes
		var active_col = active_index % sprite_details.hframes
		active_frame.set_region(Rect2( active_col * frame_width, active_row * frame_height, frame_width, frame_height ))
		frames.add_frame("active", active_frame)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var current_anim =  $AnimatedSprite2D.animation
	if state == interactable_state.ACTIVE and current_anim != "active":
		$AnimatedSprite2D.play("active")
	elif state != interactable_state.ACTIVE and current_anim == "active":
		$AnimatedSprite2D.play("default")
	
	var frames = ($AnimatedSprite2D as AnimatedSprite2D).sprite_frames
	var texture =frames.get_frame_texture($AnimatedSprite2D.animation, $AnimatedSprite2D.frame)
	var left = position.x - (texture.get_width() / 2.0)
	var right = left + texture.get_width()
	var bottom = position.y
	var top = bottom - texture.get_height()
	var currentPlayers = get_tree().get_nodes_in_group("CurrentPlayer");
	var collides = false
	for player in currentPlayers:
		var pLeft = player.position.x;
		var pBottom = player.position.y;
		if pLeft > left && pLeft < right && pBottom < bottom && pBottom > top:
			collides = true; 
	
	if collides:
		modulate.a = 0.5
	else:
		modulate.a = 1

func _on_BaseInteractable_input_event(_viewport, event, _shape_idx):
	if  event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_RIGHT:
		on_interact()
	elif event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_LEFT:
		on_break()
		

# When the timer finishes we finalize the conversion of input items to output items
func _on_Timer_timeout():
	state = interactable_state.COMPLETE
	$Timer.stop()
	
	# Clear out the input_array
	for container in input_items:
		container.count = 0
		#container.call_deffered("free")
	input_items.clear()
	
	# Create the output items, the last is displayed
	var to_create = current_recipe.item_ids_on_complete
	for item_id in to_create.keys():
		var count = to_create[item_id]
		var new_output = item_container_ref.instantiate()
		new_output.set_items(item_id, count)
		completed_items.append(new_output)
		$StorageSlot.set_item_container(new_output)
		$StorageSlot.visible = true
	
	if completed_items.size() == 0:
		state = interactable_state.PASSIVE
