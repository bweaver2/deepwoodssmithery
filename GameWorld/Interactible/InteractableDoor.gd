extends StaticBody2D


signal scene_transition_requested(scene_path, position)

@export var target_scene_path = "res://GameWorld/swamp.tscn"
@export var target_position = Vector2.ZERO
@export var locked = false
@export var locked_message = "This door is locked"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# If the user right clicks on the door, it will signal a request to change the scene
func _on_StaticBody2D_input_event(viewport, event, shape_idx):
	if  event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_RIGHT:
		if locked:
			print(locked_message)
		else:
			emit_signal("scene_transition_requested", target_scene_path, target_position)
