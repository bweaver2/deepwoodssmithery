extends "res://GameWorld/Levels/BaseLevel.gd"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# All we're doing is passing the signal along up to the next level
func _on_StaticBody2D_scene_transition_requested(scene_path, position):
	emit_signal("scene_transition_requested", scene_path, position)
