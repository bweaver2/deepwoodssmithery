extends "res://GameWorld/Levels/BaseLevel.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# All we're doing is passing the signal along up to the next level
func _on_StaticBody2D_scene_transition_requested(scene_path, position):
	emit_signal("scene_transition_requested", scene_path, position)


func _on_HammerStarter_popup_requested(scene_path):
	emit_signal("popup_requested", scene_path)


func _on_Door_scene_transition_requested(scene_path, position):
	pass # Replace with function body.


func _on_Door_input_event(viewport, event, shape_idx):
	pass # Replace with function body.
