@tool
extends TileSet


const GRASS = 5
const DIRT = 6
const DIRT_WALLS = 2
const WATER = 1
var binds = {
	GRASS: [WATER, DIRT],
	DIRT: [DIRT_WALLS],
	DIRT_WALLS: [],
	WATER: []
}

func _is_tile_bound(id, neighbour_id):
	if id in binds:
		return neighbour_id in binds[id]
	return false
