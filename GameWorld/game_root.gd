extends Node

# Stores the current map for swapping it out
var current_scene
var current_popup

# Called when the node enters the scene tree for the first time.
func _ready():
	var starting_pos = Vector2.ZERO
	starting_pos.x = 50
	starting_pos.y = 50
	change_scene_and_position("res://GameWorld/swamp.tscn", starting_pos)


func add_node_to_current_scene(new_node):
	current_scene.add_child(new_node)

func add_popup_scene(scene_path):
	print("root add popup scene")
	var popup_resource = load(scene_path)
	var popup = popup_resource.instantiate()
	
	if(current_popup):
		remove_child(current_popup)
		current_popup.call_defferred("free")
	
	current_popup = popup
	add_child(popup)
	

func change_scene_and_position(next_level_path, next_position):
	# Remove the current level scene if there is one
	if(current_scene):
		$Node2D.remove_child(current_scene)
		current_scene.call_deferred("free")
	
	# Add the next level scene to the tree
	var next_level_resource = load(next_level_path)
	var next_level = next_level_resource.instantiate()
	current_scene = next_level
	$Node2D.add_child(next_level)
	
	# Make sure we're listing for scene changes from the new level
	next_level.connect("scene_transition_requested", Callable(self, "change_scene_and_position"))
	next_level.connect("popup_requested", Callable(self, "add_popup_scene"))
	
	# Update the player's position in the newly loaded scene
	if next_position:
		$Node2D/Player.position = next_position
