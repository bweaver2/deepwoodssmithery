extends Node2D


#Every level should  have these events to handle passing them up to the root
signal scene_transition_requested(scene_path, position)
signal popup_requested(scene_path)


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
