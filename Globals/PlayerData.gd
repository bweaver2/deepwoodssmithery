extends Node

const INVENTORY_COL_COUNT = 12

signal hotbar_updated
signal inventory_updated

var item_container_ref = load("res://Globals/ItemContainer.tscn")

# The hotbar also uses the col count above
var hotbar_items = []
var selected_index = 0

# When a user clicks on an item in their inventory, 
# it needs to be held and be tied to the mouse.
var held_item = null

# The inventory is separate from the hotbar, and can be expanded via upgrades
var inventory_rows = 1
var inventory = []

var current_health = 100
var max_health = 100

var current_energy = 100
var max_energy = 100

# Eventually we will need to tie in the player customization system into here
var hair_id = 0
var shirt_id = 0
var skin_color_id = 0
var eye_color = Color.BROWN
var hair_color = Color.LIGHT_GOLDENROD
var pants_color = Color.BLACK

#TODO: Store experience, blueprints


func selected_item_container():
	var index = clamp(selected_index, 0, 11)
	return hotbar_items[index]


func auto_add_items(item_id, item_count):
	# We need to traverse the whole collection (hotbar and inventory)
	# so we save the first free slot we find and where it is
	var first_free_slot = -1
	var in_inventory = false
	
	var stack_size = ItemManager.get_item_stack_size(item_id)
	
	# first try to add it to the hotbar
	for index in range(hotbar_items.size()):
		var next = hotbar_items[index]
		if first_free_slot == -1 and (next == null or next.count == 0):
			first_free_slot = index
		
		if next != null and next.item_id == item_id and next.count > 0:
			var max_can_add = stack_size - next.count
			var count_to_add = min(max_can_add, item_count)
			
			if count_to_add > 0:
				next.add_count(count_to_add)
				item_count -= count_to_add
				emit_signal("hotbar_updated")
			
			if item_count == 0: 
				return 0
	
	# then try to add it to the inventory
	for index in range(inventory.size()):
		var next = inventory[index]
		if first_free_slot == -1 and (next == null or next.count == 0):
			first_free_slot = index
			in_inventory = true
		
		if next != null and next.item_id == item_id and next.count > 0:
			var max_can_add = stack_size - next.count
			var count_to_add = min(max_can_add, item_count)
			
			if count_to_add > 0:
				next.add_count(count_to_add)
				item_count -= count_to_add
				emit_signal("inventory_updated")
			
			if item_count == 0: 
				return 0
	
	# Try to add it to a free slot. 
	if  first_free_slot >= 0:
		var count_to_add = min(stack_size, item_count)
		item_count -= count_to_add
		if in_inventory:
			inventory[first_free_slot].set_items(item_id, count_to_add)
			emit_signal("inventory_updated")
		else:
			hotbar_items[first_free_slot].set_items(item_id, count_to_add)
			emit_signal("hotbar_updated")
		
		# This should prevent weird stack sizes from missing up here
		if item_count == 0:
			return 0
		else:
			return auto_add_items(item_id, item_count)
	
	# If we get here then we counldn't add everything, so we return what's left over
	return item_count

# Right now just makes sure each slot in the player's inventory and hotbar exist
func _ready():
	for _index in range(INVENTORY_COL_COUNT):
		hotbar_items.append(item_container_ref.instantiate())
		for _sub_index in range(inventory_rows):
			inventory.append(item_container_ref.instantiate())
			
	# TODO: load data from save file


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
