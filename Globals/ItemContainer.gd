extends Node


signal item_updated


@export var count = 0
@export var item_id = 0


func set_count(new_count):
	if new_count != count:
		count = new_count
		emit_signal("item_updated")


func add_count(new_count):
	count += new_count		
	emit_signal("item_updated")
	

func set_items(new_item_id, new_count):
	if new_item_id == item_id:
		set_count(new_count)
	else:
		item_id = new_item_id
		count = new_count
		emit_signal("item_updated")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
