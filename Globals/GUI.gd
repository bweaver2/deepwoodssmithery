extends Node2D


var storage_slot_ref = load("res://Menus/StorageSlot.tscn")
var inventory_popup_ref = load("res://Menus/StorageMenu.tscn")
var in_game_menu_ref = load("res://Menus/InGameMenu.tscn")
var tool_ref = load("res://GameWorld/Tools/tool.tscn")

var is_inventory_open = false
var inventory_instance = null

var in_game_menu_instance = null
var is_in_game_menu_open = false

# Called when the node enters the scene tree for the first time.
func _ready():
	var toolbar_slots = PlayerData.INVENTORY_COL_COUNT
	
	PlayerData.connect("hotbar_updated", Callable(self, "update_hotbar_data"))
	
	for index in range(toolbar_slots):
		var new_slot = storage_slot_ref.instantiate()
		
		var items_for_slot = PlayerData.hotbar_items[index]
		if items_for_slot != null:
			new_slot.set_item_container(items_for_slot)
		
		if(index == PlayerData.selected_index):
			new_slot.set_selected(true)
		
		$CanvasLayer/Control/HBoxContainer.add_child(new_slot)
		new_slot.connect("slot_selected", Callable(self, "on_slot_selected").bind(index))
		

func update_hotbar_data():
	for index in range(PlayerData.hotbar_items.size()):
		var next = PlayerData.hotbar_items[index];
		var display = $CanvasLayer/Control/HBoxContainer.get_child(index)
		if next == null:
			display.item_container.set_items(-1, 0)
		else:
			display.item_container.set_items(next.item_id, next.count)

func on_slot_selected(index):
	if(index < 0): 
		index = PlayerData.INVENTORY_COL_COUNT - 1
	
	index = index % PlayerData.INVENTORY_COL_COUNT
	
	var prev_index = PlayerData.selected_index
	if index == prev_index:
		return
	
	var prev_selection = $CanvasLayer/Control/HBoxContainer.get_child(prev_index)
	prev_selection.set_selected(false)
	
	var new_selection = $CanvasLayer/Control/HBoxContainer.get_child(index)
	new_selection.set_selected(true)
	
	PlayerData.selected_index = index


func on_toggle_inventory():
	
	if not is_inventory_open:
		# Show the popup
		inventory_instance = inventory_popup_ref.instance()
		$CanvasLayer.add_child(inventory_instance)
		inventory_instance.popup()
		is_inventory_open = true
		#get_tree().paused = true
		
		#hide the rest of the UI
		$CanvasLayer/Control.hide()
	else:
		inventory_instance.call_deferred("free")
		is_inventory_open = false
		#get_tree().paused = false
		
		#bring back the rest of the UI
		$CanvasLayer/Control.show()


func on_toggle_main_menu():
	if not is_in_game_menu_open:
		# Show the popup
		in_game_menu_instance = in_game_menu_ref.instance()
		$CanvasLayer.add_child(in_game_menu_instance)
		in_game_menu_instance.popup()
		is_in_game_menu_open = true
		get_tree().paused = true

		
		#hide the rest of the UI
		$CanvasLayer/Control.hide()
	else:
		in_game_menu_instance.call_deferred("free")
		is_in_game_menu_open = false
		get_tree().paused = false
		
		#bring back the rest of the UI
		$CanvasLayer/Control.show()

func on_try_use_item():
	var currentPlayers = get_tree().get_nodes_in_group("CurrentPlayer");
	if(currentPlayers.size() == 0):
		return null
	
	var currentPlayer = currentPlayers[0];
	
	var index = PlayerData.selected_index
	if (index < 0):
		return
		
	var item = PlayerData.hotbar_items[index];
	if(item == null):
		return
	
	var def = ItemManager.item_definitions[item.item_id]
	
	var theTool = tool_ref.instantiate()
	currentPlayer.add_child(theTool)
	currentPlayer.playAnimation("swing_down", true)
	
	
func on_try_alt_use_item():
	pass

func _unhandled_input(event):
	
	var just_pressed = event.is_pressed() and not event.is_echo()
	if just_pressed == false:
		return
	
	if  event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_WHEEL_UP:
		on_slot_selected(PlayerData.selected_index - 1)
	elif event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
		on_slot_selected(PlayerData.selected_index + 1)
	elif Input.is_action_just_pressed("ui_hotbar_1"):
		on_slot_selected(0)
	elif Input.is_action_just_pressed("ui_hotbar_2"):
		on_slot_selected(1)
	elif Input.is_action_just_pressed("ui_hotbar_3"):
		on_slot_selected(2)
	elif Input.is_action_just_pressed("ui_hotbar_4"):
		on_slot_selected(3)
	elif Input.is_action_just_pressed("ui_hotbar_5"):
		on_slot_selected(4)
	elif Input.is_action_just_pressed("ui_hotbar_6"):
		on_slot_selected(5)
	elif Input.is_action_just_pressed("ui_hotbar_7"):
		on_slot_selected(6)
	elif Input.is_action_just_pressed("ui_hotbar_8"):
		on_slot_selected(7)
	elif Input.is_action_just_pressed("ui_hotbar_9"):
		on_slot_selected(8)
	elif Input.is_action_just_pressed("ui_hotbar_10"):
		on_slot_selected(9)
	elif Input.is_action_just_pressed("ui_hotbar_11"):
		on_slot_selected(10)
	elif Input.is_action_just_pressed("ui_hotbar_12"):
		on_slot_selected(11)
	elif Input.is_action_just_pressed("ui_inventory"):
		on_toggle_inventory()
	elif Input.is_action_just_pressed("ui_main_menu"):
		on_toggle_main_menu()
	elif Input.is_action_just_pressed("ui_use_item"):
		on_try_use_item()
	elif Input.is_action_just_pressed("ui_alt_use_itemi"):
		on_try_alt_use_item()
