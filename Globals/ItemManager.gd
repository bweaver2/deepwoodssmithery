extends Node

const STACK_SIZE = 99

var item_container_ref = load("res://Globals/ItemContainer.tscn")

var resource_map = {
	0: { "id": 0, "sprite_sheet": "res://GameWorld/Resources/Ores.png", "texture": preload("res://GameWorld/Resources/Ores.png"), "vframes": 1, "hframes": 7, },
	1: { "id": 1, "sprite_sheet": "res://GameWorld/Interactible/Buildables.png", "texture": preload("res://GameWorld/Interactible/Buildables.png"), "vframes": 1, "hframes": 3, },
	2: { "id": 2, "sprite_sheet": "res://GameWorld/Resources/OakTree.png", "texture": preload("res://GameWorld/Resources/OakTree.png"), "vframes": 1, "hframes": 1, },
	3: { "id": 3, "sprite_sheet": "res://GameWorld/Resources/wood.png", "texture": preload("res://GameWorld/Resources/wood.png"), "vframes": 1, "hframes": 1, },
	4: { "id": 4, "sprite_sheet": "res://GameWorld/Resources/ore-nodes.png", "texture": preload("res://GameWorld/Resources/ore-nodes.png"), "vframes": 1, "hframes": 10, },
	5: { "id": 5, "sprite_sheet": "res://GameWorld/Tools/SwingTest.png", "texture": preload("res://GameWorld/Tools/SwingTest.png"), "vframes": 1, "hframes": 1, },
}

var item_definitions = {
	-1: { "id": -1, "name": "Item Not Found!", "title": "Uhh.. We couldn't find the real item", "resource_id": 0, "sprite_index": 0},
	0: { "id": 0, "name": "Copper Ore", "title": "A chunk of copper ore", "resource_id": 4, "sprite_index": 1},
	1: { "id": 1, "name": "Tin Ore", "title": "A chunk of tin ore", "resource_id": 4, "sprite_index": 2},
	2: { "id": 2, "name": "Iron Ore", "title": "A chunk of iron ore", "resource_id": 4, "sprite_index": 3},
	3: { "id": 3, "name": "Gold Ore", "title": "A chunk of gold ore", "resource_id": 4, "sprite_index": 4},
	4: { "id": 4, "name": "Silver Ore", "title": "A chunk of silver ore", "resource_id": 4, "sprite_index": 5},
	5: { "id": 5, "name": "Cinnabar", "title": "A chunk of quicksilver ore", "resource_id": 4, "sprite_index": 6},
	6: { "id": 6, "name": "Lead Ore", "title": "A chunk of lead ore", "resource_id": 4, "sprite_index": 7},
	7: { "id": 7, "name": "Clay Bloomery", "title": "A basic smelter for all base metals", "resource_id": 1, "sprite_index": 0, "stack_size": 1, "placeable": true, "buildable_id": 0},
	8: { "id": 8, "name": "Wood", "title": "A piece of wood", "resource_id": 3, "sprite_index": 0},
	9: { "id": 9, "name": "Nickel Ore", "title": "A chunk of nickel ore", "resource_id": 4, "sprite_index": 8},
	10: { "id": 10, "name": "Stone", "title": "A chunk of stone", "resource_id": 4, "sprite_index": 0},
	11: { "id": 11, "name": "SwingTest", "title": "A generic item for swinging", "resource_id": 4, "sprite_index": 0, "stack_size": 1, "is_tool": true},
}

var interactable_definitions = {
	-1: { "id": -1, "name": "Interactable not found!", "Definition": "Uhh... we couldn't find the real interactable", "resource_id": 1, "sprite_index": 0, "active_sprite_indexes": [1,2], "dropped_on_destroy": [], "recipe_ids": [0]},
	0: { "id": 0, "name": "Clay Bloomery", "Definition": "A basic smelter for all base metals", "resource_id": 1, "sprite_index": 0, "active_sprite_indexes": [1,2], "dropped_on_destroy" : [7], "recipe_ids": [0,1]},
	1: { "id": 1, "name": "Oak Tree", "Definition": "Drops acorns and wood", "resource_id": 2, "sprite_index": 0, "active_sprite_indexes": [], "dropped_on_destroy" : [{8:4}], "recipe_ids": []},
}

var recipes_definitions = {
	0: { "item_ids_required": { 0 : 2 }, "item_ids_on_complete": { 1 : 1}, "time_to_make": 5},
	1: { "item_ids_required": { 1 : 2 }, "item_ids_on_complete": { 2 : 1}, "time_to_make": 5}
}

func get_stack_size_for_item(item_id):
	var item_def = item_definitions[item_id]
	if item_def != null and "stack_size" in item_def:
		return item_def.stack_size
	return STACK_SIZE

func get_accepted_items_for_interactable_recipes(interactable_id):
	var def = interactable_definitions[interactable_id]
	if def == null:
		def = interactable_definitions[-1]
	
	var accepted_item_ids = []
	
	var recipe_ids = def.recipe_ids
	for recipe_id in recipe_ids:
		var recipe = recipes_definitions[recipe_id]
		var items_needed = recipe.item_ids_required
		for item_id in items_needed:
			accepted_item_ids.append(item_id)
	
	return accepted_item_ids
	
func get_is_simple_interactable(interactable_id):
	var def = interactable_definitions[interactable_id]
	if def == null:
		def = interactable_definitions[-1]
	
	var accepted_item_ids = []
	
	var recipe_ids = def.recipe_ids
	for recipe_id in recipe_ids:
		var recipe = recipes_definitions[recipe_id]
		var items_needed = recipe.item_ids_required
		if items_needed.size() > 1:
			return false
	
	# if we didn't find a recipe needing more than 1 item, then it's simple
	return true

func get_sprites_for_item(item_id):
	var item_def = item_definitions[item_id]
	if item_def == null:
		item_def = item_definitions[-1]
	
	# See if we already have the texture loaded
	var resource_def = resource_map[item_def.resource_id]
	if not "texture" in resource_def:
		resource_def.texture = load(resource_def.sprite_sheet)
	
	return { 
		"sprite_index": item_def.sprite_index, 
		"vframes": resource_def.vframes, 
		"hframes": resource_def.hframes,
		"texture": resource_def.texture
	}
	
func get_item_stack_size(item_id):
	var item_def = item_definitions[item_id]
	if item_def == null:
		item_def = item_definitions[-1]
	
	if not "stack_size" in item_def:
		return STACK_SIZE
	else:
		return item_def.stack_size
		
	
func get_details_for_interactable(interactable_id):
	var interactable_def = interactable_definitions[interactable_id]
	if interactable_def == null:
		interactable_def = interactable_definitions[-1]
	
	# Make sure the texture has been loaded
	var resource_def = resource_map[interactable_def.resource_id]
	if not "texture" in resource_def:
		resource_def.texture = load(resource_def.sprite_sheet)
		
	return {
		"sprite_index": interactable_def.sprite_index, 
		"active_sprite_indexes": interactable_def.active_sprite_indexes, 
		"vframes": resource_def.vframes, 
		"hframes": resource_def.hframes,
		"texture": resource_def.texture
	}

func get_current_player_item_container():
	# I'm trying to prepare for the future if there are multiple players
	# Each connected client should only ever have a single CurrentPlayer
	var currentPlayers = get_tree().get_nodes_in_group("CurrentPlayer");
	if(currentPlayers.size() == 0):
		return null
		
	var currentPlayer = currentPlayers[0];
	var item_id = currentPlayer.current_item_container
	return item_id



func _init():
	for resource_id in resource_map:
		var resource_def = resource_map[resource_id]
		resource_def.texture = load(resource_def.sprite_sheet)
