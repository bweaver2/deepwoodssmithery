extends Node

enum seasons { SPRING = 0, SUMMBER = 1, AUTUMN = 2, WINTER = 3 }
enum maps { TOWN = 0, SWAMP = 1 }

# Time in the game
var game_year = 0
var game_season = seasons.SPRING
var game_day = 0

# We need a place to store all of the items placed by players
# In the form of { maps.TOWN: { xcoord: 0, ycoord: 0, interactable_id: 0, data: any}
var placed_items = {}

#TODO: City TODOs and partial completions need to be stored
# ie the player has dropped off 6 Nails but 0 Joists for the Mill



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
