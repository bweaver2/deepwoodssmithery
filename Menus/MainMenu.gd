extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_NewGameButton_pressed():
	assert(get_tree().change_scene_to_file("res://current_scene.tscn") == OK, "Couldn't Start a new game")


func _on_OptionsButton_pressed():
	pass # Replace with function body.


func _on_LoadButton_pressed():
	pass # Replace with function body.


func _on_ExitButton_pressed():
	get_tree().notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)
