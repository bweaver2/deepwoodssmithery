extends PopupPanel



var storage_slot_ref = load("res://Menus/StorageSlot.tscn")


func on_slot_selected(selection):
	print(selection)

# Called when the node enters the scene tree for the first time.
func _ready():
	var col_count = PlayerData.INVENTORY_COL_COUNT
	
	$GridContainer.columns = col_count
	
	for index in range(col_count):
		var new_slot = storage_slot_ref.instantiate()
		
		var items_for_slot = PlayerData.hotbar_items[index]
		if items_for_slot != null:
			new_slot.set_item_container(items_for_slot)
		
		$GridContainer.add_child(new_slot)
		
	for index in range(PlayerData.inventory.size()):
		var new_slot = storage_slot_ref.instantiate()
		
		var items_for_slot = PlayerData.inventory[index]
		if items_for_slot != null:
			new_slot.set_item_container(items_for_slot)
		
		$GridContainer.add_child(new_slot)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
