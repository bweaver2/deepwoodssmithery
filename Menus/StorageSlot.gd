extends Panel

signal slot_selected

@export var is_selected = false

var storage_slot_ref = load("res://Menus/StorageSlot.tscn")
var item_container_ref = load("res://Globals/ItemContainer.tscn")
var item_container = null
	

# On init, we need to make sure we have an existing item_container
# and are listening for changes
func _init():
	item_container = item_container_ref.instantiate()
	item_container.connect("item_updated", Callable(self, "update_display"))
	
	
func set_selected(new_is_selected):
	is_selected = new_is_selected
	update_display()
	
	
func set_item_container(new_item_container):
	item_container = new_item_container
	# Make sure we're listing for changes from the item container
	new_item_container.connect("item_updated", Callable(self, "update_display"))
	update_display()


# When we attempt to drag, we provide the following data
func _get_drag_data(_pos):
	# Use another StorageSlot as drag preview.
	var to_be_dragged = storage_slot_ref.instantiate()
	to_be_dragged.set_item_container(item_container)
	set_drag_preview(to_be_dragged)
	
	# We pass a reference to our item container
	return item_container

# Drag/Drop needs to be limited to something with an item_container
func _can_drop_data(_pos, data):
	return "item_id" in data

# When the drop happens, we're doing a swap of contents 
func _drop_data(_pos, data):
	var my_item_id = item_container.item_id
	var my_count = item_container.count
	
	#If they're the same, try to combine
	if my_item_id == data.item_id:
		var max_count = ItemManager.get_stack_size_for_item(my_item_id)
		var combined = my_count + data.count
		if(combined > max_count):
			item_container.set_count(max_count)
			data.set_count(combined - max_count)
		else:
			item_container.set_count(combined)
			data.set_count(0)
	else:
		item_container.set_items(data.item_id, data.count)
		data.set_items(my_item_id, my_count)


func update_display():
	
	var item_sprite = ItemManager.get_sprites_for_item(item_container.item_id)
	var count = item_container.count
	$Label.text = str(count)
	$ItemSprite.texture = item_sprite.texture
	$ItemSprite.vframes = item_sprite.vframes
	$ItemSprite.hframes = item_sprite.hframes
	$ItemSprite.frame = item_sprite.sprite_index
	
	var image_size = item_sprite.texture.get_size() #image size
	var desired_size = 16.0
	var x_scale = desired_size / (image_size.x / item_sprite.hframes)
	var y_scale = desired_size / (image_size.y / item_sprite.vframes)
	var new_scale = Vector2(x_scale, y_scale)
	$ItemSprite.scale = new_scale
	
	if count <= 0:
		$ItemSprite.visible = false
		$Label.visible = false
	elif count == 1:
		$ItemSprite.visible = true
		$Label.visible = false
	elif count > 1:
		$ItemSprite.visible = true
		$Label.visible = true
		
	if not is_selected:
		var styles = get_theme_stylebox("panel").duplicate()
		styles.border_color = Color(0.388235, 0.145098, 0.145098)
		add_theme_stylebox_override("panel", styles)
	else:
		var styles = get_theme_stylebox("panel").duplicate()
		styles.border_color = Color(0.870588, 0.862745, 0.388235)
		add_theme_stylebox_override("panel", styles)

# Called when the node enters the scene tree for the first time.
func _ready():
	update_display()



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_StorageSlot_gui_input(event):
	if  event is InputEventMouseButton and event.pressed and (event.button_index == MOUSE_BUTTON_RIGHT or event.button_index == MOUSE_BUTTON_LEFT):
		emit_signal("slot_selected")
		
