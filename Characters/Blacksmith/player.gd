extends CharacterBody2D

#var velocity = Vector2.ZERO

const  MAX_SPEED = 400
const ACCELERATION = 10
const FRICTION = 10;

@export var magnet_size = 40.0;
@export var magnet_strength = 100.0;

var current_item_container = null
var is_animation_locked = false

func can_pickup_items(item_id, item_count):
	#PlayerData.can_add_one_of_item(item_id)
	return true

func playAnimation(animation_str, is_locked = false):
	is_animation_locked = is_locked
	$AnimatedSprite2D.play(animation_str)

func pickup_items(item_id, item_count):
	return PlayerData.auto_add_items(item_id, item_count)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	if (is_animation_locked == true):
		return
	
	var x_dir = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	var y_dir = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")

	if x_dir > 0:
		$AnimatedSprite2D.play("right")
	elif x_dir < 0:
		$AnimatedSprite2D.play("left")
	elif y_dir < 0:
		$AnimatedSprite2D.play("up")
	elif y_dir > 0:
		$AnimatedSprite2D.play("down")	
	else:
		$AnimatedSprite2D.stop()
		$AnimatedSprite2D.frame = 0


func _physics_process(delta):
	#if (is_animation_locked == true):
	#	return
	
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	input_vector = input_vector.normalized()
	
	#lerp gives us some softer turns instead of harsh jumps in direction
	if input_vector != Vector2.ZERO:
		velocity = lerp(velocity, input_vector * MAX_SPEED, ACCELERATION * delta)
	else:
		velocity = lerp(velocity, Vector2.ZERO, FRICTION * delta)
		#If they're close enough to zero, just stop
		if velocity.length() < (MAX_SPEED / 20.0):
			velocity = Vector2.ZERO
	
	if velocity.length() > 0:
		if !$WalkingSounds.playing:
			$WalkingSounds.play()
	else:
		$WalkingSounds.stop()
	
	
	set_velocity(velocity)
	move_and_slide()


func _on_AnimatedSprite_animation_finished():
	if (is_animation_locked == true):
		is_animation_locked = false
